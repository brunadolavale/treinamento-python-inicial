#Funcoes

def calcula(val1, val2, func):
	return func(val1, val2)

def multiplica(a, b):
	return a * b

def soma(a, b):
	return a + b

opt = "multiplica"

if opt == "soma":
	print(calcula(10,7, soma))
elif opt == "multiplica":
	print(calcula(10,7, multiplica))
else:
	pass