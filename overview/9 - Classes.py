# Classe/objeto

class Cachorro:
	name = "Rex"

	def __init__(self):
		self.name = "Half"

obj = Cachorro()
obj.name = "Totózinho"
print(obj.name)

class Doc(object):
	name = ""
	def __init__(self, name):
		self.name = name

a = Doc("Ruf")
print(a.name)

class Cachorro(object):
	def __init__(self):
		print("Instancia")
	
	def latir(self, ofthen):
		print("Au! " * ofthen)

class SaoBernardo(Cachorro):
	def latir(self, ofthen = 1):
		print("Woof" * ofthen)

c = Cachorro()
c.latir(10)