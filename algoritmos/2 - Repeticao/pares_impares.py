'''
Verifique dentre uma lista de numeros, quias sao pares e quais sao impares
Armazene a incidencia em um uma lista de dicionarios.
'''

pares = 0
impares = 0
dicionario_numeros = {}
lista = []
for i in range(1, 6):
    numero = int(input('Informe um numero: '))
    if (numero % 2 == 0):
        pares += 1
        dicionario_numeros["pares"] = pares
    else:
    	impares += 1
    	dicionario_numeros["impares"] = impares

print(dicionario_numeros)

