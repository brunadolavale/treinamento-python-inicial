paes = [
	{"id": 0, "item": "italiano", "preco": 10},
	{"id": 1, "item": "parmesao", "preco": 12},
	{"id": 2, "item": "integral", "preco": 15},
]

recheios = [
	{"id": 0, "item": "frango", "preco": 4.3},
	{"id": 1, "item": "carne", "preco": 5.1},
	{"id": 2, "item": "bacon", "preco": 7.5},
]

class RealizarPedido:

	def selecionar_item(self, lista):
		for item in lista:
			print(item)
		_id = input("\n Escolha o id do item desejado: ")
		item_selecionado = lista[int(_id)]
		
		return item_selecionado


	def valida_escolha(self, lista):
		preco = 0
		item = self.selecionar_item(lista)
		preco += item["preco"]
		return preco


	def forma_de_pagamento(self, preco):
		pagamento = input("Forma de pagamento? ")
		if pagamento == "dinheiro":
			desconto = preco * 0.1
			preco = preco - desconto
		elif pagamento == "debito":
			desconto = preco * 0.05
			preco = preco - desconto
		else:
			pass

		return preco


	def calcular_valor_final(self):
		valor_pao = float(self.valida_escolha(paes))
		valor_recheio = float(self.valida_escolha(recheios))
		valor_total = valor_pao + valor_recheio
		valor_final = self.forma_de_pagamento(valor_total)
		
		return "O valor final eh de: "+ str(valor_final)

pedido = RealizarPedido()
print(pedido.calcular_valor_final())