paes = [
	{"id": 0, "item": "italiano", "preco": 10},
	{"id": 1, "item": "parmesao", "preco": 12},
	{"id": 2, "item": "integral", "preco": 15},
]

recheios = [
	{"id": 0, "item": "frango", "preco": 4.3},
	{"id": 1, "item": "carne", "preco": 5.1},
	{"id": 2, "item": "bacon", "preco": 7.5},
]

Crie um algoritmo que contabilize o valor final de um sanduíche, levando em consideração o preço de cada item selecionado.

O cliente deve inserir no sistema as seguintes informações:
	- Tipo do pão
	- Tipo do recheio
	- Forma de pagamento

Caso o pagamento seja feito no dinheiro, o cliente recebe um desconto de 10%;
Caso seja feito no débito, o desconto é de 5%;
Do contrário, nenhum desconto será dado.
--------
for pao in paes:
	print(pao["id"])


##CODIGO DO RODRIGO
paes = [	{"id": 0, "item": "italiano", "preco": 10},
	{"id": 1, "item": "parmesao", "preco": 12},
	{"id": 2, "item": "integral", "preco": 15},]

recheios = [	{"id": 0, "item": "frango", "preco": 4.3},
	{"id": 1, "item": "carne", "preco": 5.1},
	{"id": 2, "item": "bacon", "preco": 7.5},]
valor = 0

tipo_pao = input('Tipo do Pão: ')

for pao in paes:
  if tipo_pao == pao["item"]:
    valor = valor + pao["preco"]

tipo_recheio = input('Recheio: ')

for recheio in recheios:
  if tipo_recheio == recheio["item"]:
    valor = valor + recheio["preco"]

forma_pagamento = input('Forma Pagamento: ')

if forma_pagamento == "debito":
  valor = valor * .95
elif forma_pagamento == "dinheiro":
  valor = valor * .90
else: valor

print(valor)



-------------------------------
pagamentos = [
	{"meio":"dinheiro", "desconto": 0.90},
	{"meio": "debito", "desconto": 0.95},
	{"meio": "outros", "desconto": 1.00}
]


def calcula_preco(pao,recheio,pagamento):
	pao_preco = [x["preco"] for x in paes if x["item"]==pao][0]
	recheios_preco = [x["preco"] for x in recheios if x["item"]==recheio][0]
	pagamento_desconto = [x["desconto"] for x in pagamentos if x["meio"]==pagamento][0]
	preco = (float(pao_preco)+recheios_preco)*pagamento_desconto
	return(preco)


---- usando dictionary comprehension ---
preco_pao = {d["item"]:d["preco"] for d in paes}[pao]
    