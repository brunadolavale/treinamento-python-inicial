lista = [1, 4, 6, 7, 10]
lista2 = ['1', '4', '6', '7', '10']

# map1
a  = list(map(lambda x: x**2, lista))
print(a)

# map2
list(map(lambda x: int(x), lista2)) #sendo lista uma lista de strings numéricas - > ['1', '3']

# List comprehension
print([int(x) for x in lista])