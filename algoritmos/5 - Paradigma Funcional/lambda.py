# lambda1
transformar = lambda x, y: x**y
transformar(3, 4) #isto retorna 81


# forma tradicional
def transformar(x, y): return x**y
transformar(3, 4) #isto também retorna 81


# lambda2
iguais = lambda x, y: "Sim" if x == y else "Nao" iguais(54, 55) #isto retorna "Nao"


# forma tradicional
def iguais(x, y): if x == y: return "Sim" else: return "Nao" iguais(54, 55) #isto também retorna "Nao"