'''
Faca um agoritmo Fibonacci recursivo
'''

def fibonacci(n):
	if n == 1 or n == 2:
		return 1
	return fibonacci(n-1) + fibonacci(n-2)

def chama_fibonacci():
	lista = []
	for i in range(1, 10):
		lista.append(fibonacci(i))
	return lista

print(chama_fibonacci())